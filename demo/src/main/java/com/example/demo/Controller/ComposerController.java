package com.example.demo.Controller;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.Model.Artist;
import com.example.demo.Model.Band;
import com.example.demo.Model.Composer;
import com.example.demo.Service.ComposerService;

@RestController
public class ComposerController {
  
    @Autowired
    ComposerService composerService;
    
    @GetMapping("all-bands")
    public ArrayList<Band> getAllBandApi(){
        ArrayList<Band> allBand = composerService.getBands();
        return allBand ;
    }

    @GetMapping("all-artists")
    public ArrayList<Artist> getArtistsAPI(){
        ArrayList<Artist> all = composerService.getArtists();
        return all ;
    }

    @GetMapping("all-composers")
    public ArrayList<Composer> getAll(){
        ArrayList<Composer> all = composerService.getComposeres();
        return all ;
    }
}
